# Dinocore

Dinocore module that allows selection and export of dino core data from the TNO data dump. See the tutorial folder for examples of usage.

![Select cores by points, lines or polygons](https://gitlab.com/deltares/tgg-projects/dinocore/-/raw/master/example_GIS.png)
