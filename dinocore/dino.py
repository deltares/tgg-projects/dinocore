# -*- coding: utf-8 -*-
"""
Dinocores module

Load, select and export Dino core data. See the Jupyter notebook for usage

For questions, feedback, suggestions:
erik.vanonselen@deltares.nl

Work-in-Progress:
    - Export to IPF in any coordinate system through keyword epsg (works for shapefile already)
    - Get core density per area selection
"""
import numpy as np
import pandas as pd
import geopandas as gpd
from collections import namedtuple
from shapely.geometry import Point
from shapely.ops import transform
from fiona.crs import from_epsg
import pyproj
from tqdm import tqdm
from pathlib import Path
from dinocore import spatial
import warnings

warnings.filterwarnings("ignore")


class Dinocores:
    """
    Class for loading, selecting and exporting DINOloket borehole data.
    
    Parameters
    ----------
    metafile : string
        Filepath of the metadata table of the boreholes.
    datafile : string
        Filepath of the data table of the boreholes.
    read_data_cols : array_like, optional
        Columns from the data table to read. The default is None, than standard
        columns are used.
    
    """
    __columns = [
        "nr",
        "x",
        "y",
        "mv",
        "end",
        "top",
        "bottom",
        "lith",
        "ak",
        "as",
        "az",
        "ag",
        "ah",
        "shfr",
        "plantfr",
        "strat_2003",
        "desc",
    ]
    
    def __init__(self, metafile, datafile, read_cols_data=None):
        print("Please wait while the data is loaded")
        self.__read_metafile(metafile)
        self.__read_datafile(datafile, read_cols_data)
        print("Data loaded. Dinocores object is ready for use\n")

        self.meta_selection = None
        self.data_selection = None

        self.data_ntup = namedtuple("data_tup", "meta data")
        self.data_map = {
            "all": self.data_ntup(self.meta, self.data),
            "sel": self.data_ntup(self.meta_selection, self.data_selection),
        }

        self.func_map = {
            ".shp": [self._sel_shp, self._to_shp],
            ".gpkg": [self._sel_shp, self._to_shp],
            ".ipf": [None, self._to_ipf],
            "LineString": self._lindist,
            "Point": self._pntdist,
            str: self._sel_id,
            list: self._sel_id,
        }

    def __read_metafile(self, metafile):
        ext = Path(metafile).suffix
        if ext == ".parquet":
            self.meta = pd.read_parquet(metafile)
        elif ext == ".csv":
            self.meta = pd.read_csv(metafile)
    
    def __read_datafile(self, datafile, readcols=None):
        if not readcols:
            readcols = self.__columns

        ext = Path(datafile).suffix
        if ext == ".parquet":
            self.data = pd.read_parquet(
                datafile,
                columns=readcols,
            )
        elif ext == ".csv":
            self.data = pd.read_csv(
                datafile,
                usecols=readcols,
                sep=",",
                engine="c",
                encoding="ISO-8859-1",
                low_memory=False,
            )

    def select(
        self,
        *query_object,
        min_len=0,
        max_len=999,
        strat=False,
        strat_to_return="any",
        lith=[],
        **kwargs,
    ):
        """
        Parameters
        ----------
        *query_object : String, List, Path object (to shapefile), np.ndarray
            - String with core id: e.g. 'B38A0001'
            - List with core ids: e.g. ['B38A0001', 'B38A0002', 'B38A0003']
            - String of Path object with path to shapefile (requires kwargs)
                NOTE:   If shapefile is a line or point, requires 'distance' kwarg
                        If shapefile is a polygon
        min_len: int or float
            Minimum length of the cores to be selected (default 0)
        max_len: int or float
            Maximum length of the cores to be selected (default 999)
        strat: bool
            When True, only selects the cores that have at least one layer
            with a stratigraphic unit assigned to it.
        lith: list
            List of lithologies (K, Z, KZ, etc)
        **kwargs :
            distance: distance to search from point or line geometry

        Returns
        -------
        self.meta_selection and self.data_selection containing the selected data

        """
        self.meta_selection = self.meta
        self.data_selection = self.data

        if query_object:
            query_object = query_object[0]

            try:
                p = Path(query_object)
                if p.is_file():
                    how = p.suffix
                    self.func_map[how][0](query_object, **kwargs)
                else:
                    how = type(query_object)
                    self.func_map[how](query_object, **kwargs)
            except TypeError:
                if type(query_object) == list:
                    if all([isinstance(x, str) for x in query_object]):
                        self.func_map[list](query_object, **kwargs)
                    elif len(query_object) == 4 and all(
                        [isinstance(x, (int, float)) for x in query_object]
                    ):
                        self._sel_bbox(query_object)
                    else:
                        print("The query input is invalid, no selection possible\n")

        # add additional filters from kwargs
        if min_len != 0 or max_len != 999:
            self._sel_len(min_len, max_len)

        if strat:
            self._sel_strat(strat_to_return=strat_to_return)

        if len(lith) != 0:
            self._sel_lith(lith)

        # Final step: update data_map
        self.data_map["sel"] = self.data_ntup(self.meta_selection, self.data_selection)

    def export(self, filename, cores="sel", **kwargs):
        p = Path(filename)

        try:
            self.func_map[p.suffix][1](p, cores, **kwargs)
        except KeyError:
            print(
                "During export: export file type was not recognised. File suffix must be .shp or .ipf\n"
            )

    def _sel_len(self, min_len, max_len):
        l = -(self.meta_selection["END"] - self.meta_selection["MV"])
        self.meta_selection = self.meta_selection.loc[
            l[(l >= min_len) & (l <= max_len)].index
        ]
        self.data_selection = self.data[self.data["nr"].isin(self.meta_selection["NR"])]

    def _sel_strat(self, strat_to_return="any"):
        if strat_to_return == "any":
            notna = self.data_selection["nr"][
                self.data_selection["strat_2003"].notna()
            ].unique()
            self.meta_selection = self.meta_selection[
                self.meta_selection["NR"].isin(notna)
            ]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]
        elif type(strat_to_return) == list:
            notna = self.data_selection["nr"][
                self.data_selection["strat_2003"].isin(strat_to_return)
            ].unique()
            self.meta_selection = self.meta_selection[
                self.meta_selection["NR"].isin(notna)
            ]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]
        else:
            print(
                "WARNING: given stratigraphy not understood. No selection based on this criteria was made"
            )

    def _sel_lith(self, lith):
        notna = self.data_selection["nr"][
            self.data_selection["lith"].isin(lith)
        ].unique()
        self.meta_selection = self.meta_selection[self.meta_selection["NR"].isin(notna)]
        self.data_selection = self.data[self.data["nr"].isin(self.meta_selection["NR"])]

    def _sel_id(self, id_strings, **kwargs):
        if type(id_strings) == str:
            self.meta_selection = self.meta[self.meta["NR"] == id_strings]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]
            if len(self.meta_selection) == 0:
                print(f"{id_strings} was not found, selection is empty")
        elif type(id_strings) == list:
            self.meta_selection = self.meta[self.meta["NR"].isin(id_strings)]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]
            if len(self.meta_selection) < len(id_strings):
                diff = len(id_strings) - len(self.meta_selection)
                print(f"{diff} IDs were not found, please check names")

    def _sel_bbox(self, bbox):
        self.meta_selection = self.meta[
            (self.meta.X >= bbox[0])
            & (self.meta.X <= bbox[1])
            & (self.meta.Y >= bbox[2])
            & (self.meta.Y <= bbox[3])
        ]
        self.data_selection = self.data[self.data["nr"].isin(self.meta_selection["NR"])]

    def _sel_shp(self, shapefile, **kwargs):
        """
        Select data from shapefile

        Parameters
        ----------
        shapefile : path (object) to shapefile
            DESCRIPTION.
        **kwargs : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        try:
            self.select_shape = gpd.read_file(shapefile).to_crs(from_epsg(28992))
        except Exception:
            self.select_shape = gpd.read_file(shapefile)
            print(
                "WARNING: Selection geometry does not have CRS, assuming it is already in target CRS!"
            )
        shp_type = self.select_shape.type[0]

        if shp_type == "Point":
            try:
                distance = kwargs["distance"]
            except KeyError:
                distance = 500
                print(
                    f"During select from {shp_type}: 'distance' kwarg missing. Set to default of 500 length units.\n"
                )
            mindist = self.func_map[shp_type]()
            idxs_to_keep = (mindist <= distance).nonzero()
            self.meta_selection = self.meta.iloc[idxs_to_keep]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]

        elif shp_type == "LineString":
            try:
                distance = kwargs["distance"]
            except KeyError:
                distance = 500
                print(
                    f"During select from {shp_type}: 'distance' kwarg missing. Set to default of 500 length units.\n"
                )
            idxs_to_keep = np.empty([0], dtype=np.int64)

            for i in range(len(self.select_shape)):
                mindist = self.func_map[shp_type](i)
                idxs_to_keep = np.hstack(
                    (idxs_to_keep, (mindist <= distance).nonzero()[0])
                )

            _, to_keep_from_keep = np.unique(idxs_to_keep, return_index=True)
            idxs_to_keep = idxs_to_keep[to_keep_from_keep]
            self.meta_selection = self.meta.iloc[idxs_to_keep]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]

        elif shp_type == "Polygon":
            try:
                buffer = kwargs["buffer"]
            except KeyError:
                buffer = 0
                print(
                    f"During select from {shp_type}: optional 'buffer' kwarg missing. Set to default of 0 length units.\n"
                )
            to_keep = self._inpoly(buffer=buffer)
            self.meta_selection = self.meta.iloc[to_keep]
            self.data_selection = self.data[
                self.data["nr"].isin(self.meta_selection["NR"])
            ]
        else:
            print("Unsupported shapefile type: no selection possible\n")

        if len(self.meta_selection) == 0:
            print(f"No cores within/close to shape found, selection is empty\n")

    def _to_shp(self, shapefile, cores, epsg=28992, **kwargs):
        geometries = []
        meta = self.data_map[cores].meta

        if epsg != 28992:
            project = pyproj.Transformer.from_proj(
                pyproj.Proj(from_epsg(28992)), pyproj.Proj(from_epsg(kwargs["epsg"]))
            )
            for x, y in zip(meta.X, meta.Y):
                geometries.append(transform(project.transform, Point([(x, y)])))
        else:
            for x, y in zip(meta.X, meta.Y):
                geometries.append(Point([(x, y)]))

        gdf = gpd.GeoDataFrame(meta)
        gdf["geometry"] = geometries

        # TODO: Get initial CRS?
        gdf.crs = from_epsg(28992)

        if epsg != 28992:
            gdf = gdf.to_crs(from_epsg(epsg))

        gdf.to_file(shapefile)

    def _to_ipf(self, ipf_file, cores, **kwargs):
        meta = self.data_map[cores].meta
        data = self.data_map[cores].data

        # Create IPF file
        with open(ipf_file, "w") as ipf:
            ipf.write(f"{len(self.meta_selection)}\n5")
            ipf.write("\nX-COORDINAAT\n")
            ipf.write("Y-COORDINAAT\n")
            ipf.write("IDENTIFICATIE\n")
            ipf.write("MAAIVELD\n")
            ipf.write("EINDDIEPTE\n")
            ipf.write("3,TXT\n")

            # Zip faster?
            for X, Y, NR, MV, END in meta[["X", "Y", "NR", "MV", "END"]].itertuples(
                index=False
            ):
                ipf.write(
                    f"'{str(X)}','{str(Y)}','{str(NR)}','{str(MV)}','{str(END)}'\n"
                )

        # Create IPF source TXT files
        grp = data.groupby("nr")

        pbar = tqdm(total=len(data["nr"].unique()), desc="Exporting to IPF...")
        for nr, df in grp:
            # df = df.iloc[::-1]
            output = ipf_file.parent.joinpath(f"{nr}.txt")

            with open(output, "w") as outfile:
                outfile.write(f"{str(len(df)+1)}")
                outfile.write("\n")
                outfile.write(f"11,2")
                outfile.write("\n")
                outfile.write('"Grensvlak, m+NAP",-999.99\n')
                outfile.write('"Lithologie",-999.99\n')
                outfile.write('"Klei bijmenging",-999.99\n')
                outfile.write('"Silt bijmenging",-999.99\n')
                outfile.write('"Zand bijmenging",-999.99\n')
                outfile.write('"Grind bijmenging",-999.99\n')
                outfile.write('"Organisch",-999.99\n')
                outfile.write('"Schelpen",-999.99\n')
                outfile.write('"Plantresten",-999.99\n')
                outfile.write('"Stratigrafie",-999.99\n')
                outfile.write('"Beschrijving",-999.99\n')

                gv = df.values[:, 5]
                lit = df.values[:, 7]
                desc = df.values[:, -1]
                ak = df.values[:, 8]
                ast = df.values[:, 9]
                az = df.values[:, 10]
                ag = df.values[:, 11]
                ah = df.values[:, 12]
                shfr = df.values[:, 13]
                plantfr = df.values[:, 14]
                strat = df.values[:, -2]
                for i in range(len(desc)):
                    desc[i] = '"' + str(desc[i]) + '"'
                    ak[i] = '"' + str(ak[i]) + '"'
                    ast[i] = '"' + str(ast[i]) + '"'
                    az[i] = '"' + str(az[i]) + '"'
                    ag[i] = '"' + str(ag[i]) + '"'
                    ah[i] = '"' + str(ah[i]) + '"'
                    shfr[i] = '"' + str(shfr[i]) + '"'
                    plantfr[i] = '"' + str(plantfr[i]) + '"'
                    strat[i] = '"' + str(strat[i]) + '"'

                towrite = np.vstack(
                    (gv, lit, ak, ast, az, ag, ah, shfr, plantfr, strat, desc)
                ).transpose()

                np.savetxt(outfile, towrite, fmt="%s", delimiter=",")
                outfile.write("%s,-,-,-,-,-,-,-,-,-,-" % (df["end"].values[0]))
            pbar.update()
        pbar.close()

    def _pntdist(self):
        """
        see upfun.distance.pntdist

        """
        pointcoords = np.array(
            [list(pnt.coords)[0] for pnt in self.select_shape.geometry], dtype=np.int64
        )
        points = self.meta[["X", "Y"]].to_numpy()

        return spatial.pntdist(pointcoords, points)

    def _lindist(self, i):
        """
        see upfun.distance.lindist

        """
        linecoords = np.array(self.select_shape.iloc[i].geometry.coords, dtype=np.int64)
        points = self.meta[["X", "Y"]].to_numpy()

        return spatial.lindist(linecoords, points)

    def _inpoly(self, buffer=0):
        # Get buffered polygon
        self.select_shape_buff = self.select_shape.copy()
        self.select_shape_buff["geometry"] = self.select_shape.buffer(distance=buffer)

        boolarrs = np.zeros([len(self.meta), len(self.select_shape_buff)])
        points = self.meta[["X", "Y"]].to_numpy()
        for i in range(len(self.select_shape_buff)):
            linecoords = np.array(
                self.select_shape_buff.exterior.values[i].coords, dtype=np.int64
            )
            boolarrs[:, i] = spatial.points_in_poly(linecoords, points)

        final_array = np.array(np.nanmax(boolarrs, axis=1), dtype=np.bool)

        return final_array
