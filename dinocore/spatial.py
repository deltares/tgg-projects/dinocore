# -*- coding: utf-8 -*-
"""
Pure numpy implementations for:
    
    - Distance between arrays of query points and data points
    - Distance between lines and data points
    - Data points in polygon based on raycasting algorythm 
    
making use of vectorised computations where possible

erik.vanonselen@deltares.nl
"""
import numpy as np


def pntdist(query_points, data_points):
    """
    Vectorised function to compute the minimum distance of data points to
    query points. Both need to be in the same cartesian coordinate system.

    Parameters
        ----------
        query_points : np.ndarray
            Numpy array of size (q,2), where q is the amount of query points.
            To tranform a shapely point geometry into the input format use:
                np.array([list(pnt.coords)[0] for pnt in geometries])
        data_points : np.ndarray
            Numpy array of size (d,2), where d is the amount of data points

    Returns
    -------
    Numpy array with size (d,1) that contains distance of a data point to
    the closest query point.

    """
    distances = []
    for xp, yp in zip(query_points[:, 0], query_points[:, 1]):
        distance = np.zeros([len(data_points)])
        distance = np.sqrt(
            (xp - data_points[:, 0]) ** 2 + (yp - data_points[:, 1]) ** 2
        )
        distances.append(distance)

    array = np.transpose(np.array(distances))
    mindist = np.nanmin(array, axis=1)
    return mindist


def lindist(query_line, data_points):
    """
    Vectorised function to compute the minimum distance of data points to a
    query line. Both need to be in the same cartesian coordinate system.

    Parameters
        ----------
        query_line : np.ndarray
            Numpy array of size (q,2), where q is the amount of query line
            points. These points represent the coordinates of the start point,
            joints and end point of the line.
            To tranform a shapely linestring geometry into the input format use:
                np.array(geometries[idx].coords)
        data_points : np.ndarray
            Numpy array of size (d,2), where d is the amount of data points

    Returns
    -------
    Numpy array with size (d,1) that contains distance of a data point to
    the query line.

    """
    # Slopes and intercepts of line segments
    dydx = np.diff(query_line[:, 1]) / np.diff(query_line[:, 0])
    intc = query_line[:-1, 1] - (dydx * query_line[:-1, 0])

    # pre-allocate arrays
    distance_from_line = np.zeros([len(data_points)])
    distance_from_endpoints = np.zeros([len(data_points), 2])
    distances = []

    # Iterate over line segments
    for i, (a, b) in enumerate(zip(dydx, intc)):
        distance = np.zeros([len(data_points)])
        xstart = np.sort(query_line[i : i + 2, 0])
        ystart = np.sort(query_line[i : i + 2, 1])
        xref = (data_points[:, 1] + (1 / a) * data_points[:, 0] - b) / (a + 1 / a)
        yref = a * xref + b
        checkpos = ((xref <= xstart[1]) & (xref >= xstart[0])).nonzero()
        distance_from_endpoints[:, 0] = np.sqrt(
            (xstart[0] - data_points[:, 0]) ** 2 + (ystart[0] - data_points[:, 1]) ** 2
        )
        distance_from_endpoints[:, 1] = np.sqrt(
            (xstart[1] - data_points[:, 0]) ** 2 + (ystart[1] - data_points[:, 1]) ** 2
        )
        min_distance_from_endpoint = np.nanmin(distance_from_endpoints, axis=1)
        distance_from_line = np.sqrt(
            (xref - data_points[:, 0]) ** 2 + (yref - data_points[:, 1]) ** 2
        )

        # Create final distance array from combining dist to line and endpoints
        mask = np.ones(len(data_points), np.bool)
        mask[checkpos] = 0

        distance[checkpos] = distance_from_line[checkpos]
        distance[mask] = min_distance_from_endpoint[mask]

        # Append to final list
        distances.append(distance)

    array = np.transpose(np.array(distances))
    mindist = np.nanmin(array, axis=1)
    return mindist


def points_in_poly(edge_lines, data_points):
    """
    Vectorised function to get points within a polygon. Based on raycasting method.

    Parameters
    ----------
    edge_lines : np array of size (n,2)
        Describes the polygon shape. Numpy array of coordinates of the corner
        points given in clockwise or anticlockwise direction
    data_points : np array of size (d,2)
        Np array of all points to be evaluated

    Returns
    -------
    Numpy array of bools with size (d,1) that contains True for points within
    the polygon and False for points outside of it

    """
    # preselect datapoints based on boundaries. Outside these boundaries it is
    # pointless to evaluate the data_points as they do not lie within the polygon
    bbox = [
        np.min(edge_lines[:, 0]),
        np.max(edge_lines[:, 0]),
        np.min(edge_lines[:, 1]),
        np.max(edge_lines[:, 1]),
    ]
    data_points_sel = (
        (data_points[:, 0] >= bbox[0])
        & (data_points[:, 0] <= bbox[1])
        & (data_points[:, 1] >= bbox[2])
        & (data_points[:, 1] <= bbox[3])
    )
    data_points_used = data_points[data_points_sel]

    # Slopes and intercepts of line segments
    dydx_edges = np.diff(edge_lines[:, 1]) / np.diff(edge_lines[:, 0])
    intc_edges = edge_lines[:-1, 1] - (dydx_edges * edge_lines[:-1, 0])

    corrects = np.zeros([len(data_points_used), len(dydx_edges)], dtype=np.int8)
    for i, (a, b) in enumerate(zip(dydx_edges, intc_edges)):

        # Intersection point between edge and ray lines
        ymatch = data_points_used[:, 1]
        xmatch = (ymatch - b) / a
        # If xmatches are nan, we are dealing with a vertical line
        if all(np.isnan(xmatch)):
            xmatch = np.full_like(xmatch, edge_lines[i, 0])

        # Range in which that intersection should be (based on extent of edge)
        yrange = np.sort(np.array([edge_lines[i, 1], edge_lines[i + 1, 1]]))

        corrects[:, i] = np.array(
            (
                (xmatch >= data_points_used[:, 0])
                & (ymatch >= yrange[0])
                & (ymatch < yrange[1])
            )
        )

    numcor = np.sum(corrects, axis=1, dtype=np.int8)
    point_in_poly = numcor % 2 != 0

    # Cast selected points back into original array
    data_points_sel[data_points_sel] = point_in_poly
    return data_points_sel
