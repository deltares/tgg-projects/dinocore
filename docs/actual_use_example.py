from dinocore import dino
from pathlib import Path

folder = Path(__file__).parent

dc = dino.dinocores(
    r"p:\430-tgg-data\DINO\DINO extractie april 2022\DINO_Boornummers_d20220405.parquet",
    r"p:\430-tgg-data\DINO\DINO extractie april 2022\DINO_Extractie_bovennaaronder_d20220405.parquet",
)

bbox = [105000, 110000, 450000, 455000]


dc.select(
    r"n:\Projects\11207500\11207576\B. Measurements and calculations\PRORAIL-76 - Review SOS Arcadis_fase1_deelopdracht3.2 dl 2\data\arcadis_do3.2 dl 2.shp",
    distance=600,
    min_len=0.5,
    lith=["V"],
)

dc.export(
    r"n:\Projects\11207500\11207576\B. Measurements and calculations\PRORAIL-76 - Review SOS Arcadis_fase1_deelopdracht3.2 dl 2\data\IPF\boringen_arcadis_do3.2 dl2_veen.ipf",
    cores="sel",
    epsg=28992,
)
dc.export(
    r"n:\Projects\11207500\11207576\B. Measurements and calculations\PRORAIL-76 - Review SOS Arcadis_fase1_deelopdracht3.2 dl 2\data\boringen_arcadis_do3.2 dl2_veen.shp",
    cores="sel",
    epsg=28992,
)

dc.data_selection.to_csv(
    r"c:\Users\onselen\Projects\Tempel Marc\dino_boringen_data.csv"
)

# Select cores within 6000 m and with a length > 5.5 m around the eyes of the smiley
dc.select(
    folder.joinpath(r"Example input\smileyeyes.shp"),
    distance=5000,
    min_len=5.5,
)

# export to shape and ipf file
dc.export(
    folder.joinpath(r"Example output\smileyeyesdata.shp"),
    cores="sel",
    epsg=28992,
)
dc.export(
    folder.joinpath(r"Example output\IPF\smileyeyesdata.ipf"),
    cores="sel",
    epsg=28992,
)

# Select cores within 15000 m and with 1 < length < 30 m around the mouth of the smiley
dc.select(
    folder.joinpath(r"Example input\smileymouth.shp"),
    distance=1500,
    min_len=1,
    max_len=30,
)

# export to shape and ipf file
dc.export(
    folder.joinpath(r"Example output\smileymouthdata.shp"),
    cores="sel",
    epsg=28992,
)
dc.export(
    folder.joinpath(r"Example output\IPF\smileymouthdata.ipf"),
    cores="sel",
    epsg=28992,
)
